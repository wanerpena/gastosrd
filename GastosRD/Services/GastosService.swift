//
//  GastosService.swift
//  GastosRD
//
//  Created by Waner Alexander Peña Sánchez on 7/28/18.
//  Copyright © 2018 Waner Alexander Peña Sánchez. All rights reserved.
//

import Foundation
import Firebase
import FirebaseStorage

class GastosService {
    static let instance = GastosService()
    
    var gastosArr = [GastosModel]()
    
    func subirFoto(foto: UIImage?, completion: @escaping ((String) -> Void)) {
        let imageName = UUID().uuidString
        let storageRef = Storage.storage().reference().child(FACTURA).child("\(imageName).png")
        
        var downloadUrl = ""
        
        if let uploadData = UIImagePNGRepresentation(foto!) {
            storageRef.putData(uploadData, metadata: nil) { (metadata, err) in
                if err != nil {
                    print(err as Any)
                    return
                }
                
                storageRef.downloadURL(completion: { (url, err) in
                    downloadUrl = (url?.absoluteString)!
                    
                    completion(downloadUrl)
                })
            }
        }
    }
    
    func registrarGasto(values: [String: Any]) {
        let ref = Database.database().reference()
        
        ref.child(GASTO).childByAutoId().updateChildValues(values) { (err, dbRef) in
            if err != nil {
                print(err as Any)
            }
            
            print(dbRef)
            ref.removeAllObservers()
        }
    }
    
    func obtenerGastoByUser(completion: @escaping ((Bool) -> Void)) {
        let uid = Auth.auth().currentUser?.uid
        let ref = Database.database().reference()
        
        ref.child(GASTO).queryOrdered(byChild: "idUsuario").queryEqual(toValue: uid).observe(.childAdded) { (snapshot) in
            
            if let data = snapshot.value as? [String: Any] {
                print(data)
                var gastos = GastosModel()
                gastos.id = snapshot.key
                gastos.idEmpresa = data["idEmpresa"] as? String
                gastos.rncSuplidor = data["rncSuplidor"] as? String
                gastos.ncf = data["ncf"] as? String
                gastos.fecha = data["fecha"] as? String
                gastos.total = Float(truncating: data["total"] as! NSNumber)
                gastos.itbis = Float(truncating: data["itbis"] as! NSNumber)
                gastos.concepto = data["concepto"] as? String
                gastos.foto = data["foto"] as? String
                
                self.gastosArr.append(gastos)
                print(self.gastosArr)
            }
            
            if snapshot.value != nil {
                completion(true)
            } else {
                completion(false)
            }
        }
        
        completion(false)
    }
}
