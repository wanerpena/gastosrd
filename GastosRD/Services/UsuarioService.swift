//
//  UsuarioService.swift
//  GastosRD
//
//  Created by Waner Alexander Peña Sánchez on 7/28/18.
//  Copyright © 2018 Waner Alexander Peña Sánchez. All rights reserved.
//

import Foundation
import Firebase

class UsuarioService {
    static let instance = UsuarioService()
    
    func registrarUsuario(email: String,pass: String, nombre: String) {
        Auth.auth().createUser(withEmail: email, password: pass) { (user, error) in
            if error == nil && user != nil {
                
                let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest()
                
                changeRequest?.displayName = nombre
                changeRequest?.commitChanges(completion: { (error) in
                    if error == nil {
                        print("Nombre colocado!")
                    }
                })
                
                self.guardarUsuario(uid: (user?.uid)!, email: (user?.email)!)
            }
        }
    }
    
    func signInUser(email: String, pass: String) {
        Auth.auth().signIn(withEmail: email, password: pass, completion: nil)
    }
    
    func guardarUsuario(uid: String, email: String) {
        let ref = Database.database().reference()
        let values = UsuarioFirebase(id: uid, email: email).dictionary
        
        ref.child(USUARIO).child(uid).updateChildValues(values) { (err, dbRef) in
            if err != nil {
                print(err as Any)
            }
            
            print(dbRef)
        }
    }
    
    func obtenerUsuario() {
        let uid = Auth.auth().currentUser?.uid
        let ref = Database.database().reference()
        
        ref.child(USUARIO).child(uid!).observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let values = snapshot.value as? [String: Any] {
                for x in values {
                    let name = x.value
                    print(name)
                }
            }
            
        }, withCancel: nil)
    }
}
