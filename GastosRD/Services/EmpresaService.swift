//
//  EmpresaService.swift
//  GastosRD
//
//  Created by Waner Alexander Peña Sánchez on 7/21/18.
//  Copyright © 2018 Waner Alexander Peña Sánchez. All rights reserved.
//

import Foundation
import Firebase

class EmpresaService {
    static let instance = EmpresaService()
    var empresaArr = [EmpresaModel]()
    
    func obtenerEmpresa(rnc : String, completion: ((Empresa?, Bool) -> Void)?) {
        
        let url = URL(string: URL_GET_CONTRIBUYENTES + rnc)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: request) { (responseData, response, responseError) in
            
            DispatchQueue.main.async {
                if let data = responseData {
                    
                    let decoder = JSONDecoder()
                    
                    do {
                        let empresa = try decoder.decode(Empresa.self, from: data)
                        
                        completion!((empresa), true)
                    } catch let error {
                        print(error)
                        completion!((nil), false)
                    }
                }
            }
        }
        
        task.resume()
    }
    
    func guardarEmpresa(nombreEmpresa: String, rnc: String) {
        let ref = Database.database().reference()
        let idUsuario = (Auth.auth().currentUser?.uid)!
        
        let values = EmpresaFirebase(nombre: nombreEmpresa, rnc: rnc, idUsuario: idUsuario).dictionary
        
        ref.child(EMPRESA).childByAutoId().updateChildValues(values) { (err, dbRef) in
            if err != nil {
                print(err as Any)
                return
            }
            
            ref.removeAllObservers()
            self.obtenerEmpresasByUsuario()
        }
    }
    
    func obtenerEmpresasByUsuario() {
        let ref = Database.database().reference()
        
        let userId = Auth.auth().currentUser?.uid
        
        ref.child(EMPRESA).queryOrdered(byChild: "idUsuario").queryEqual(toValue: userId).observe(.childAdded) { (snapshot) in
            
            if let data = snapshot.value as? [String: Any] {
                
                let empresa = EmpresaModel()
                empresa.rnc = data["rnc"] as? String
                empresa.idUsuario = data["idUsuario"] as? String
                empresa.nombre = data["nombre"] as? String
                
                self.empresaArr.append(empresa)
            }
        }
    }
    
    func obtenerEmpresaByNombre(nombre: String, completion: @escaping ((EmpresaModel?) -> Void)) {
        let ref = Database.database().reference()
        
        ref.child(EMPRESA).queryOrdered(byChild: "nombre").queryEqual(toValue: nombre).observe(.childAdded) { (snapshot) in
            
            if let data = snapshot.value as? [String: Any] {
                let empresa = EmpresaModel()
                empresa.id = snapshot.key
                empresa.rnc = data["rnc"] as? String
                empresa.idUsuario = data["idUsuario"] as? String
                empresa.nombre = data["nombre"] as? String
                
                completion(empresa)
            }
        }
    }
    
    func obtenerNombreEmpresaByIdEmpresa(uid: String, completion: @escaping ((String) -> Void)) {
        let ref = Database.database().reference()
        
        ref.child(EMPRESA).child(uid).observeSingleEvent(of: .value) { (snapshot) in
            if let data = snapshot.value as? [String: Any] {
                print(data)
                let nombre = data["nombre"] as! String
                completion(nombre)
            }
        }
    }
}
