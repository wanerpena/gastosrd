//
//  Constants.swift
//  GastosRD
//
//  Created by Waner Alexander Peña Sánchez on 7/21/18.
//  Copyright © 2018 Waner Alexander Peña Sánchez. All rights reserved.
//

import Foundation

let BASE_URL = "http://adamix.net/gastosrd/api.php?act="
let URL_GET_CONTRIBUYENTES = "\(BASE_URL)GetContribuyentes&rnc="

let HEADER = [
    "Content-Type": "application/json; charset=utf-8"
]

// Models
let EMPRESA = "Empresas"
let USUARIO = "Usuarios"
let GASTO = "Gastos"
let FACTURA = "Facturas"

