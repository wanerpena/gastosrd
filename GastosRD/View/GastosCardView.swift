//
//  GastosCardView.swift
//  GastosRD
//
//  Created by Waner Alexander Peña Sánchez on 7/21/18.
//  Copyright © 2018 Waner Alexander Peña Sánchez. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class GastosCardView: UIView {

    @IBInspectable var cornerRadius : CGFloat = 5
    @IBInspectable var shadowColor : UIColor? = .black
    
    @IBInspectable let shadowOffSetWidth : Int = 0
    @IBInspectable let shadowOffSetHeight : Int = 1
    @IBInspectable let shadowOpacity : Float = 0.5
    
    override func layoutSubviews() {
        //layer.backgroundColor = UIColor.gray.cgColor
        layer.cornerRadius = cornerRadius
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffSetWidth, height: shadowOffSetHeight)
        
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        layer.shadowPath = shadowPath.cgPath
        
        layer.shadowOpacity = shadowOpacity
    }

}
