//
//  GastosViewCell.swift
//  GastosRD
//
//  Created by Waner Alexander Peña Sánchez on 7/21/18.
//  Copyright © 2018 Waner Alexander Peña Sánchez. All rights reserved.
//

import UIKit

class GastosViewCell: UITableViewCell {

    @IBOutlet weak var lblFecha: UILabel!
    @IBOutlet weak var lblNcf: UILabel!
    @IBOutlet weak var lblEmpresa: UILabel!
    @IBOutlet weak var rncSuplidor: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    
    func configureCell(gasto: GastosModel) {
        EmpresaService.instance.obtenerNombreEmpresaByIdEmpresa(uid: gasto.idEmpresa!) { (nombreEmpresa) in
            
            self.lblFecha.text = gasto.fecha!
            self.lblNcf.text = gasto.ncf!
            self.lblEmpresa.text = nombreEmpresa
            self.rncSuplidor.text = gasto.rncSuplidor!
            self.lblTotal.text = String(gasto.total!)
        }
    }
}
