//
//  UsuarioViewController.swift
//  GastosRD
//
//  Created by Waner Alexander Peña Sánchez on 7/22/18.
//  Copyright © 2018 Waner Alexander Peña Sánchez. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class UsuarioViewController: UITableViewController, UITextFieldDelegate {
    @IBOutlet weak var txtNombre: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPass1: UITextField!
    @IBOutlet weak var txtPass2: UITextField!
    @IBOutlet weak var navigationBar: UINavigationBar!
    
    var isModal = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtNombre.delegate = self
        txtEmail.delegate = self
        txtPass1.delegate = self
        txtPass2.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if isModal {
            self.navigationItem.title = "Registro de usuario"
            let button = UIBarButtonItem(title: "Registrar", style: .done, target: self, action: #selector(registrar))
            
            let close = UIBarButtonItem(title: "Cerrar", style: .done, target: self, action: #selector(self.cerrarViewController))
            
            self.navigationItem.rightBarButtonItem = button
            self.navigationItem.leftBarButtonItem = close
            navigationBar.setItems([self.navigationItem], animated: true)
        } else {
            setBarItems()
        }
    }
    
    @objc func cerrarViewController() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setBarItems() {
        self.navigationItem.title = "Registro de usuario"
        let button = UIBarButtonItem(title: "Registrar", style: .done, target: self, action: #selector(registrar))
        
        self.navigationItem.rightBarButtonItem = button
        navigationBar.setItems([self.navigationItem], animated: true)
    }
    
    @objc func registrar() {
        guard let nombre = txtNombre.text else { return }
        guard let email = txtEmail.text else { return }
        guard let pass1 = txtPass1.text else { return }
        guard let pass2 = txtPass2.text else { return }
        
        if nombre != "" && email != "" && pass1 != "" && pass2 != "" {
            if pass1 == pass2 {
                UsuarioService.instance.registrarUsuario(email: email, pass: pass1, nombre: nombre)
                
                if isModal {
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
