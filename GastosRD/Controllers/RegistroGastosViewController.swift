//
//  RegistroGastosViewController.swift
//  GastosRD
//
//  Created by Waner Alexander Peña Sánchez on 7/21/18.
//  Copyright © 2018 Waner Alexander Peña Sánchez. All rights reserved.
//

import UIKit
import Firebase

class RegistroGastosViewController: UITableViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var txtEmpresa: UITextField!
    @IBOutlet weak var txtRnc: UITextField!
    @IBOutlet weak var txtNcf: UITextField!
    @IBOutlet weak var txtFecha: UITextField!
    @IBOutlet weak var txtTotal: UITextField!
    @IBOutlet weak var txtItbis: UITextField!
    @IBOutlet weak var txtConcepto: UITextField!
    @IBOutlet weak var btnHoy: UIButton!
    @IBOutlet weak var btnAyer: UIButton!
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var imageViewFactura: UIImageView!
    
    var fechaSelected = ""
    
    let datePicker = UIDatePicker()
    
    let blueColor = UIColor(red: 0.0 / 255.0, green: 194.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0)
    
    var empresas: [String] = ["Select..."]
    
    let empresaPickerView = UIPickerView()
    
    var empresaSelected : String?
    
    var idEmpresa = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtEmpresa.delegate = self
        
        empresaPicker()
        toolBarEmpresaPicker()
        
        showDatePicker()
        setNavigationItem()
        setImageViewSelector()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        EmpresaService.instance.obtenerEmpresasByUsuario()
        cargarEmpresas()
    }
    
    func cargarEmpresas() {
        for i in EmpresaService.instance.empresaArr {
            empresas.append(i.nombre!)
        }
    }
    
    func empresaPicker() {
        empresaPickerView.delegate = self
        empresaPickerView.backgroundColor = .white
        
        txtEmpresa.inputView = empresaPickerView
    }
    
    func toolBarEmpresaPicker() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        toolBar.barTintColor = blueColor
        toolBar.tintColor = .white
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self,
                                         action: #selector(self.dismissKeyBoard))
        
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        txtEmpresa.inputAccessoryView = toolBar
    }
    
    @objc func dismissKeyBoard() {
        view.endEditing(true)
    }
    
    func setImageViewSelector() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(seleccionarImagen))
        imageViewFactura.addGestureRecognizer(tap)
        imageViewFactura.isUserInteractionEnabled = true
        imageViewFactura.contentMode = .scaleAspectFill
        imageViewFactura.clipsToBounds = true
    }
    
    func setNavigationItem() {
        let button = UIBarButtonItem(title: "Registrar", style: .done, target: self, action: #selector(registrarGasto))
        
        navigationItem.title = "Registro de Gastos"
        navigationItem.rightBarButtonItem = button
        navigationBar.setItems([navigationItem], animated: true)
    }
    
    @objc func registrarGasto() {
        guard let empresa = txtEmpresa.text else { return }
        guard let rnc = txtRnc.text else { return }
        guard let ncf = txtNcf.text else { return }
        guard let fecha = txtFecha.text else { return }
        guard let total = txtTotal.text else { return }
        guard let itbis = txtItbis.text else { return }
        guard let concepto = txtConcepto.text else { return }
        
        if Auth.auth().currentUser?.uid != nil {
            if empresa != "" && rnc != "" && ncf != "" && total != "" && itbis != "" {
                
                EmpresaService.instance.obtenerEmpresaByNombre(nombre: empresa) { (empresa) in
                    self.idEmpresa = (empresa?.id)!
                }
                
                let idUsuario = (Auth.auth().currentUser?.uid)!
                
                if let imagen = imageViewFactura.image {
                    GastosService.instance.subirFoto(foto: imagen) { (url) in
                        self.fechaSelected = fecha != "" ? fecha : self.fechaSelected
                        
                        let values = GastosFirebase(idEmpresa: self.idEmpresa, rncSuplidor: rnc, ncf: ncf, fecha: self.fechaSelected, total: Float(total)!, itbis: Float(itbis)!, foto: url, concepto: concepto, idUsuario: idUsuario).dictionary
                        
                        GastosService.instance.registrarGasto(values: values)
                    }
                }
            } else {
                let alert = UIAlertController(title: "Aviso", message: "Debe completar todos los campos para continuar.", preferredStyle: .alert)
                
                let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
                
                alert.addAction(ok)
                
                present(alert, animated: true, completion: nil)
            }
        } else {
            let alert = UIAlertController(title: "Aviso", message: "Debe haber un usuario logueado para poder registrar un un gasto.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            alert.addAction(ok)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.endEditing(true)
    }
    
    @objc func seleccionarImagen() {
        let imagePikcer = UIImagePickerController()
        imagePikcer.allowsEditing = true
        imagePikcer.sourceType = .photoLibrary
        imagePikcer.delegate = self
        
        present(imagePikcer, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        var selectedImagePicker : UIImage?
        
        if let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            selectedImagePicker = originalImage
        } else if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            selectedImagePicker = editedImage
        }
        
        if let selectedImage = selectedImagePicker {
            imageViewFactura.image = selectedImage
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func selectedDate(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            fechaSelected = Date().dateString
            
            btnHoy.backgroundColor = blueColor
            btnHoy.tintColor = .white
            
            btnAyer.backgroundColor = .white
            btnAyer.tintColor = blueColor
        case 1:
            fechaSelected = Date().yesterday.dateString
            
            btnAyer.backgroundColor = blueColor
            btnAyer.tintColor = .white
            
            btnHoy.backgroundColor = .white
            btnHoy.tintColor = blueColor
        default:
            break
        }
    }
    
    func showDatePicker() {
        //Format Date
        datePicker.datePickerMode = .date
        datePicker.backgroundColor = .white
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneDatePicker))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker))
        
        toolbar.setItems([doneButton, spaceButton, cancelButton], animated: false)
        toolbar.barTintColor = blueColor
        toolbar.tintColor = .white
        
        // add toolbar to textField
        txtFecha.inputAccessoryView = toolbar
        // add datepicker to textField
        txtFecha.inputView = datePicker
        
    }
    
    @objc func doneDatePicker() {
        //dismiss date picker dialog
        self.view.endEditing(true)
        //For date format
//        let formatter = DateFormatter()
//        formatter.dateFormat = "dd/MM/yyyy"
        //txtFecha.text = formatter.string(from: datePicker.date)
        txtFecha.text = datePicker.date.dateString
        
        self.btnHoy.isHidden = true
        self.btnAyer.isHidden = true
    }
    
    @objc func cancelDatePicker() {
        //cancel button dismiss datepicker dialog
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }*/
    
}

extension Date {
    var yesterday: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: self)!
    }
    var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: self)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month, from: self)
    }
    var isLastDayOfMonth: Bool {
        return tomorrow.month != month
    }
    var dateString: String {
        
        let date = Date()
        
        let formatter = DateFormatter()
        
        formatter.dateFormat = "dd/MM/yyyy"
        
        let dateString = formatter.string(from: date)
        
        return dateString
    }
}

extension RegistroGastosViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return empresas.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return empresas[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        empresaSelected = empresas[row]
        
        txtEmpresa.text = empresaSelected
    }
}
