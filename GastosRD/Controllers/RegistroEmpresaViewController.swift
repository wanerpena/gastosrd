//
//  RegistroEmpresaViewController.swift
//  GastosRD
//
//  Created by Waner Alexander Peña Sánchez on 7/21/18.
//  Copyright © 2018 Waner Alexander Peña Sánchez. All rights reserved.
//

import UIKit
import Firebase

class RegistroEmpresaViewController: UITableViewController, UITextFieldDelegate {

    @IBOutlet weak var txtRnc: UITextField!
    @IBOutlet weak var lblEmpresa: UILabel!
    @IBOutlet weak var navigation: UINavigationItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        txtRnc.delegate = self
        
        self.tableView.tableFooterView = UIView(frame: .zero)
        
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        lblEmpresa.text = ""
        txtRnc.text = ""
        
        setNavigationBar()
    }
    
    func setNavigationBar() {
        navigation.title = "Registrar Empresa"
        var signInOutBtn : UIBarButtonItem
        
        let button = UIBarButtonItem(title: "Registrar", style: .done, target: self, action: #selector(registrarEmpresa))
        
        if Auth.auth().currentUser?.uid != nil {
            signInOutBtn = UIBarButtonItem(title: "Log Out", style: .plain, target: self, action: #selector(logOut))
        } else {
            signInOutBtn = UIBarButtonItem(title: "Sign In", style: .plain, target: self, action: #selector(signIn))
        }
        
        navigation.rightBarButtonItem = button
        navigation.leftBarButtonItem = signInOutBtn
    }
    
    @objc func logOut() {
        do {
            if Auth.auth().currentUser?.uid != nil {
                try Auth.auth().signOut()
                
                var signInOutBtn : UIBarButtonItem
                
                signInOutBtn = UIBarButtonItem(title: "Sign In", style: .plain, target: self, action: #selector(signIn))
                
                navigation.leftBarButtonItem = signInOutBtn
            }
        } catch let error {
            print(error)
        }
    }
    
    @objc func signIn() {
        let email = "wanerpena@gmail.com"
        let pass = "ps041125"
        
        if Auth.auth().currentUser?.uid == nil {
            UsuarioService.instance.signInUser(email: email, pass: pass)
            
            var signInOutBtn : UIBarButtonItem
            
            signInOutBtn = UIBarButtonItem(title: "Log Out", style: .plain, target: self, action: #selector(logOut))
            
            navigation.leftBarButtonItem = signInOutBtn
        }
    }
    
    @objc func registrarEmpresa() {
        guard let rnc = txtRnc.text else { return }
        guard let descripcion = lblEmpresa.text else { return }
        
        if Auth.auth().currentUser?.uid != nil {
            if rnc != "" && descripcion != "" {
                EmpresaService.instance.guardarEmpresa(nombreEmpresa: descripcion, rnc: rnc)
                
                txtRnc.text = ""
                lblEmpresa.text = ""
            } else {
                let alert = UIAlertController(title: "Aviso", message: "Debe introducir un RNC y consultar el nombre para poder registrar una empresa.", preferredStyle: .alert)
                
                let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
                
                alert.addAction(ok)
                
                self.present(alert, animated: true)
            }
        } else {
            let alert = UIAlertController(title: "Aviso", message: "Debe haber un usuario logueado para registrar una empresa.", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
            
            alert.addAction(ok)
            
            self.present(alert, animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Consultar(_ sender: UIButton) {
        guard let rnc = txtRnc.text else { return }
        
        if rnc != "" {
            EmpresaService.instance.obtenerEmpresa(rnc: rnc) { (empresa, valido) in
                if valido {
                    self.lblEmpresa.text = empresa?.RGE_NOMBRE
                    self.dismissKeyboard()
                } else {
                    let alert = UIAlertController(title: "Aviso", message: "No se ha encontrado ninguna empresa con el RNC suministrado.", preferredStyle: .alert)
                    let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
                    
                    alert.addAction(ok)
                    
                    self.present(alert, animated: true)
                }
            }
        } else {
            let alert = UIAlertController(title: "Aviso", message: "Debe introducir un RNC.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
    
            alert.addAction(ok)
    
            self.present(alert, animated: true)
        }
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtRnc.endEditing(true)
        return true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
