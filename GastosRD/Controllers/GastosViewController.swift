//
//  ViewController.swift
//  GastosRD
//
//  Created by Waner Alexander Peña Sánchez on 7/21/18.
//  Copyright © 2018 Waner Alexander Peña Sánchez. All rights reserved.
//

import UIKit
import GoogleSignIn
import Firebase

class GastosViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var navigation: UINavigationBar!
    @IBOutlet weak var tableView: UITableView!
    
    let test : [String] = ["Hola", "Bien", "K Ase"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorColor = .clear
        tableView.tableFooterView = UIView(frame: .zero)
        // Do any additional setup after loading the view, typically from a nib.
        setNavigationItem()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        cargarDatos()
    }
    
    func cargarDatos() {
        GastosService.instance.obtenerGastoByUser { (success) in
            if success {
                if EmpresaService.instance.empresaArr.count > 0 {
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }
    
    func setNavigationItem() {
        let navigationItem = UINavigationItem()
        navigationItem.title = "Gastos"
        
        navigation.setItems([navigationItem], animated: true)
    }
    
    func verificarUsuarioLogeuado() {
        if Auth.auth().currentUser?.uid == nil {
            performSelector(inBackground: #selector(logOut), with: nil)
        }
    }
    
    @objc func logOut() {1
        do {
            try Auth.auth().signOut()
        } catch let error {
            print(error)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension GastosViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return test.count //GastosService.instance.gastosArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if GastosService.instance.gastosArr.count > 0 {
            if let cell = Bundle.main.loadNibNamed("GastosViewCell", owner: self, options: nil)?.first as? GastosViewCell {
                print(GastosService.instance.gastosArr.count)
                print(GastosService.instance.gastosArr)
                print(indexPath.row)
                let index = indexPath.row - 2
                let gasto = GastosService.instance.gastosArr[index]
                
                cell.configureCell(gasto: gasto)
                
                return cell
            } else {
                return UITableViewCell()
            }
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 150
    }
}

