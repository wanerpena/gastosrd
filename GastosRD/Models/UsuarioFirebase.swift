//
//  UsuarioFirebase.swift
//  GastosRD
//
//  Created by Waner Alexander Peña Sánchez on 7/27/18.
//  Copyright © 2018 Waner Alexander Peña Sánchez. All rights reserved.
//

import Foundation

struct UsuarioFirebase {
    let id : String
    let email : String
    var dictionary : [String: Any] {
        return ["id": id,
                "email": email]
    }
}
