//
//  GastosModel.swift
//  GastosRD
//
//  Created by Waner Alexander Peña Sánchez on 7/29/18.
//  Copyright © 2018 Waner Alexander Peña Sánchez. All rights reserved.
//

import UIKit

struct GastosModel {
    var id : String?
    var idEmpresa : String?
    var rncSuplidor : String?
    var ncf : String?
    var fecha : String?
    var total : Float?
    var itbis : Float?
    var foto : String?
    var concepto : String?
    var idUsuario : String?
}
