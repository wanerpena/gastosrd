//
//  EmpresaModel.swift
//  GastosRD
//
//  Created by Waner Alexander Peña Sánchez on 7/28/18.
//  Copyright © 2018 Waner Alexander Peña Sánchez. All rights reserved.
//

import Foundation
import UIKit

class EmpresaModel: NSObject {
    var id : String? = nil
    var rnc : String? = nil
    var idUsuario : String? = nil
    var nombre : String? = nil
}
