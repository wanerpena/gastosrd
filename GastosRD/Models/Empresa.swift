//
//  Empresa.swift
//  GastosRD
//
//  Created by Waner Alexander Peña Sánchez on 7/21/18.
//  Copyright © 2018 Waner Alexander Peña Sánchez. All rights reserved.
//

import Foundation

struct Empresa: Codable {
    let RGE_RUC : String
    let RGE_NOMBRE : String?
    let NOMBRE_COMERCIAL : String
    let CATEGORIA : String
    let REGIMEN_PAGOS : String
    let ESTATUS : String
}
