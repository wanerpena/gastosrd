//
//  GastosFirebase.swift
//  GastosRD
//
//  Created by Waner Alexander Peña Sánchez on 7/28/18.
//  Copyright © 2018 Waner Alexander Peña Sánchez. All rights reserved.
//

import Foundation

struct GastosFirebase {
    let idEmpresa : String
    let rncSuplidor : String
    let ncf : String
    let fecha : String
    let total : Float
    let itbis : Float
    let foto : String?
    let concepto : String?
    let idUsuario : String
    
    var dictionary : [String: Any] {
        return ["idEmpresa": idEmpresa,
                "rncSuplidor": rncSuplidor,
                "ncf": ncf,
                "fecha": fecha,
                "total": total,
                "itbis": itbis,
                "foto": foto,
                "concepto": concepto,
                "idUsuario": idUsuario]
    }
}
